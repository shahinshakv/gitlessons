const express = require('express')
const app = express()
const port = 2000
const bodyParser = require('body-parser')
const {findUser, createUser, updateUser, deleteUser,} =require('./routes.js')

app.use(bodyParser.json())
app.get('/', (req, res) => res.send("WELCOME"))
app.get('/about', (req, res) => res.send('about the page'))


app.post('/user',createUser)
app.get('/user',findUser)
app.put('/user',updateUser)
app.delete('/user',deleteUser)
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
